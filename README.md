# cliente

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
### Some images of the project 

#### Creación de Eventos
![Creacion_de_Eventos](/uploads/2bcd82a9c6e9f826393d124cf8fb9ed3/Creacion_de_Eventos.png)

#### Confirmación de Creación de Eventos
![Confirmación_de_Creación_de_Eventos](/uploads/0edc050a4a363bdffa8629561787ff1b/Confirmación_de_Creación_de_Eventos.png)

#### Detalles de Evento
![Detalles_de_Evento](/uploads/bed2cf07bc2436a7c8c393874ab3da0b/Detalles_de_Evento.png)

#### Mis Eventos
![Vista_Mis_Eventos](/uploads/2346f77b45f634584c211dbe07a34736/Vista_Mis_Eventos.png)

#### Invitación a Participar en Evento
![Invitacion_a_participar_en_evento](/uploads/e3dae634dc131b423fff569c82fe15b1/Invitacion_a_participar_en_evento.png)

