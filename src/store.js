import Vue from 'vue'
import Vuex from "vuex"
import router from './router'
import axios from 'axios'
import { startOfDay } from 'date-fns'
/* global gapi */ 
Vue.use(Vuex)

const alert = {
    namespaced: true,
    state:{
        type: null,
        message: null
    },
    actions:{
        success({ commit }, message) {
            commit('success', message);
        },
        error({ commit }, message) {
            commit('error', message);
        },
        clear({ commit }, message) {
            commit('success', message);
        }
    },
    mutations:{
        success(state, message) {
            state.type = 'alert-success';
            state.message = message;
        },
        error(state, message) {
            state.type = 'alert-danger';
            state.message = message;
        },
        clear(state) {
            state.type = null;
            state.message = null;
        }
    }
};

// se obtienen todos los datos de localStorage
const user = JSON.parse(localStorage.getItem('user'));

const account = {
    namespaced: true,
    state: {
        stateUser: user ? { status: { loggedIn: true }, user: user} : { status: { loggedOut: true }, user: null}
    },
    getters: {  
        isAdmin (state) {
          return state.stateUser.user.isAdmin
        },
        check_organizer(state){
            return state.stateUser.user.isOrganizer
        }
    },
    mutations: { 
        loginRequest(state, user) {
            state.stateUser.status = { loggingIn: true };
            state.stateUser.user = user;
        },
        loginSuccess(state, user) {
            state.stateUser.status = { loggedIn: true };
            state.stateUser.user = user;
        },
        loginFailure(state) {
            state.stateUser.status = { loggedIn: false, loggedOut: true };
            state.stateUser.user = null;
        },
        logout(state) { 
            state.stateUser.status = { loggedOut: true };
            state.stateUser.user = null;
        },
        admin(state, _isAdmin){
            state.stateUser.user.isAdmin = _isAdmin;
        },
        set_permission(state, permission){
            state.stateUser.user.isOrganizer = permission;
        }
    },
    actions: { 
        login({ dispatch, commit }, { username, password }) {
            commit('loginRequest', { username });

            // se llama al servicio de login!
            axios.post ('/login',  {
                "username":   username,
                "password":   password
            })
            .then(response => {
                console.log(response);
                if(response.data.status == "ok"){
                    
                    let usr = {
                        username: username,
                        password: password,
                        isAdmin: response.data.isAdmin,
                        isOrganizer: response.data.isOrganizer
                    }

                    commit('loginSuccess', usr);                    
                    commit('admin', response.data.isAdmin);

                    localStorage.setItem('user', JSON.stringify({ 
                                username: username,
                                password: password,
                                isAdmin: response.data.isAdmin,
                                isOrganizer: response.data.isOrganizer
                    }))
                    
                    router.replace({path: '/Home'})
                }else{
                    dispatch('alert/error', "Usuario o contraseña incorrectos", { root: true });
                    commit('loginFailure', true);
                }
            }) 
            .catch(error=> {
                console.log(error);
            });
            
        },
        logout({ commit }) {
            localStorage.removeItem('user');
            // para salir de la sesion de google!
            var auth2 = gapi.auth2.getAuthInstance();
            if (auth2) {
                auth2.signOut().then(function () {
                    console.log('User signed out.');
                });
            }
            
            commit('logout');
            router.push({path: '/Login'});
        },
        google_login({ dispatch, commit }, {username, token}){
            commit('loginRequest', { username });

            return axios.post('/google_login', {
                "username": username,
                "token": token
            })
            .then(response => {
                console.log(response.data);
                if (response.data.status == "ok") {
                    let usr = {
                        username: username,
                        token: token,
                        isAdmin: response.data.isAdmin,
                        isOrganizer: response.data.isOrganizer
                    }

                    commit('loginSuccess', usr);
                    commit('admin', response.data.isAdmin);

                    localStorage.setItem('user', JSON.stringify({
                        username: username,
                        token: token,
                        isAdmin: response.data.isAdmin,
                        isOrganizer: response.data.isOrganizer
                    }))

                    router.replace({ path: '/Home' })
                } else {
                    dispatch('alert/error', "Usuario o contraseña incorrectos", { root: true });
                    commit('loginFailure', true);
                }
            })
            .catch(error => {
                console.log(error);
            });
        },
        update_permission({commit}, username){
            axios.post('/check_organizer', {
                "username": username
            })
            .then((response) => {
                console.log(response.data)
                commit('set_permission', response.data.is_organizer)
            })
            .catch(e => {
                console.log(e)
            })
        }
    }
};
/* 
CONSTANTE DE TIEMPO QUE USAREMOS EN TODO EL SISTEMA !!!  
*/
const time = {
    namespaced: true,
    state: {
        now: new Date()
    },
    getters: {
        today (state) {
            return startOfDay(state.now)
        }
    },
    actions: {
        set_date({commit}, new_date){
            commit('setTime', new_date)
        }
    },
    mutations: {
        updateTime(state) {
            state.now = new Date
        },
        setTime(state, date){
            state.now = new Date(date)
            console.log("desde mutations");
            console.log(state.now.toISOString())
        }   
    }
};

const event_creation = {
    namespaced: true,
    state: {
        global_event: {
            name: '',
            description: '',
            vacancies: '',
            location: '',
            type: null,
            amount: '',
            keywords: [],
            members: [],
            phases: [],
            dateFirst: '',
            dayCfP: '',
            dateLast: '',
            fileProgramme: null,
            fileImage: null
        }
    },
    getters: { 
        get_event (state) {
            return state.global_event;
        }
    },
    actions: {
        clean_event_data({ commit }) {
            commit('clean_event')
        },
        set_event_data({commit}, event){
            commit('set_event',event)
        }
    },
    mutations: {
        set_event(state, event){
            state.global_event.name = event.name;
            state.global_event.description = event.description;
            state.global_event.vacancies = event.vacancies;
            state.global_event.location = event.location;
            state.global_event.type = event.type;
            state.global_event.amount = event.amount;
            state.global_event.dateFirst = event.dateFirst;
            state.global_event.dayCfP = event.dayCfP;
            state.global_event.dateLast = event.dateLast;
            state.global_event.keywords = event.keywords;
            state.global_event.members = event.members;
            state.global_event.phases = event.phases;
            state.global_event.fileProgramme = event.fileProgramme;
            state.global_event.fileImage = event.fileImage;
        },
        clean_event(state) {
            state.global_event.name = '';
            state.global_event.description = '';
            state.global_event.vacancies = '';
            state.global_event.location = '';
            state.global_event.type = null;
            state.global_event.amount = '';
            state.global_event.dateFirst = '';
            state.global_event.dayCfP = '';
            state.global_event.dateLast = ''; 
            state.global_event.keywords = [];
            state.global_event.members = [];
            state.global_event.phases = [];
            state.global_event.fileProgramme = null;
            state.global_event.fileImage = null;
        } 
    }

};

const store = new Vuex.Store({
    modules: {
      account, alert, time, event_creation
    }
});
  
export default store;
