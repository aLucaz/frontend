import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// Configuración Bootstrap-Vue
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue);

//spinners!!! :v
import {VueSpinners} from '@saeris/vue-spinners'
Vue.use(VueSpinners);

// se agregan .css de los frameworks a usar
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// NPM
import Vuelidate from 'vuelidate'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueSweetalert2 from 'vue-sweetalert2';

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';

/**/ 
import GSignInButton from 'vue-google-signin-button';
Vue.use(GSignInButton);

Vue.use(VueSweetalert2);

//Url global , MODIFICAR DE ACUERDO A DÓNDE CORRE TU FRONTEND
axios.defaults.baseURL = 'http://localhost:8080';

//Configuracion global
Vue.use(VueAxios, axios);
Vue.use(Vuelidate);

Vue.config.productionTip = false

import {func} from './global_functions.js';
Vue.prototype.$func = func;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
