function convert(d){
    // Converts the date in d to a date-object. The input can be:
    //   a date object: returned without modification
    //   an array     : Interpreted as [year,month,day]. NOTE: month is 0-11.
    //   a number     : Interpreted as number of milliseconds
    //                  since 1 Jan 1970 (a timestamp) 
    //   a string     : Any format supported by the javascript engine, like
    //                  "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
    //   an object    : Interpreted as an object with year, month and date
    //                  attributes.  **NOTE** month is 0-11.
    return (
        d.constructor === Date ? d :
        d.constructor === Array ? new Date(d[0],d[1],d[2]) :
        d.constructor === Number ? new Date(d) :
        d.constructor === String ? new Date(d) :
        typeof d === "object" ? new Date(d.year,d.month,d.date) :
        NaN
    );
}

export const func = {
    getMM: (d) =>{
        var date = convert(d);
        return date.getMonth() + 1;
    },
    dd_x_mm: (d) => {
        var arr = d.split('/',3);
        var dd = arr[0];
        var mm = arr[1];
        var aa = arr[2];
        return mm + '/' + dd + '/' + aa
    },
    format_date: (d) => {
        var date = convert(d);
        var monthNames = [
            "Enero", "Febrero", "Marzo",
            "Abril", "Mayo", "Junio", "Julio",
            "Agosto", "Septiembre", "Octubre",
            "Noviembre", "Diciembre"
        ];
    
        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();
    
        return day + ' de ' + monthNames[monthIndex] + ' del ' + year;
    },
    cmp_date: (d1, d2) => {
        // Compare two dates (could be of any type supported by the convert
        // function above) and returns:
        //  -1 : if a < b
        //   0 : if a = b
        //   1 : if a > b
        // NaN : if a or b is an illegal date
        // NOTE: The code inside isFinite does an assignment (=).
        var date1 = convert(d1).valueOf();
        var date2 = convert(d2).valueOf();
        return (
            isFinite(date1) &&
            isFinite(date2) ?
            (date1 > date2) - (date1 < date2) :
            NaN
        );
    },
    in_range: (d, start, end) => {
        // Checks if date in d is between dates in start and end.
        // Returns a boolean or NaN:
        //    true  : if d is between start and end (inclusive)
        //    false : if d is before start or after end
        //    NaN   : if one or more of the dates is illegal.
        // NOTE: The code inside isFinite does an assignment (=).
        var date = convert(d).valueOf();
        var start_date = convert(start).valueOf();
        var end_date = convert(end).valueOf();
        return (
            isFinite(date) &&
            isFinite(start_date) &&
            isFinite(end_date) ?
            start_date <= date && date <= end_date :
            NaN
        );
    }
 }