import Vue from 'vue'
import Router from 'vue-router' 

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'homeLogout',
      component: () => import('./views/EditPhases.vue')
    },
    {
      path: '/Home',
      name: 'homeLogged',
      component: () => import('./views/HomeLogged.vue')
    },
    {
      path: '/Register',
      name: 'register',
      component: () => import('./views/Register.vue')
    },
    {
      path: '/Login',
      name: 'login',
      component: () => import('./views/Login.vue')
    },
    {
      path: '/Profile',
      name: 'profile',
      component: () => import('./views/Profile.vue')
    },
    {
      path: '/MyEvents',
      name: 'myEvents',
      component: () => import( './views/MyEvents.vue')
    },
    {
      path: '/CreateEvent',
      name: 'createEvent',
      component: () => import('./views/CreateEvent.vue')
    },
    {
      path: '/ConfirmEvent',
      name: 'confirmEvent',
      component: () => import('./views/ConfirmEvent.vue')
    },
    {
      path: '/EditEvent/:id',
      name: 'editEvent',
      component: () => import('./views/EditEvent.vue')
    },
    {
      path: '/EditPhases/:id',
      name: 'editPhases',
      component: () => import('./views/EditPhases.vue')
    },
    {
      path: '/ManageUsers',
      name: 'manageUsers',
      component: () => import('./views/ManageUsers.vue')
    },
    {
      path: '/EventDetail/:id',
      name: 'eventDetail',
      component: () => import('./views/EventDetail.vue')
    },
    {
      path: '/AssignEvaluators/:id/:event_id/:id_phase',
      name: 'assignEvaluators',
      component: () => import('./views/AssignEvaluators.vue')
    },
    {
      path: '/PaperList/:id/:id_phase',
      name: 'paperList',
      component: () => import('./views/PaperList.vue')
    },
    {
      path: '/PaperListReview/:id',
      name: 'paperListReview',
      component: () => import('./views/PaperListReview.vue')
    }, 
    {
      path: '/PresidentPaperReview/:event_id/:paper_id',
      name: 'presidentPaperReview',
      component: () => import('./views/PresidentPaperReview.vue')
    }, 
    {
      path: '/PresidentPaperReviewed/:event_id/:paper_id',
      name: 'presidentPaperReviewed',
      component: () => import('./views/PresidentPaperReviewed.vue')
    }, 
    {
      path: '/SelectPaper/:id',
      name: 'selectPaper',
      component: () => import('./views/SelectPaper.vue')
    },
    {
      path: '/EvaluatePaper/:id_evaluation',
      name: 'evaluatePaper',
      component: () => import('./views/EvaluatePaper.vue')
    },
    {
      path: '/EvaluatedPaper/:id_evaluation',
      name: 'evaluatedPaper',
      component: () => import('./views/EvaluatedPaper.vue')
    },
    {
      path: '/UploadDocPostulant/:id',
      name: 'uploadDocPostulant',
      component: () => import('./views/UploadDocPostulant.vue')
    },
    {
      path: '/PaperUpload/:id/:id_phase/:needs',
      name: 'paperUpload',
      component: () => import('./views/PaperUpload.vue')
    },
    {
      path: '/MyPapers/:id',//aqui va username del usuario
      name: 'myPapers',
      component: () => import('./views/MyPapers.vue')
    },
    {
      path: '/TimeConfig',//aqui va username del usuario
      name: 'timeConfig',
      component: () => import('./views/TimeConfig.vue')
    },
     {
      path: '/UpdatePaperUpload/:id/:id_phase/:needs',
      name: 'updatePaperUpload',
      component: () => import('./views/UpdatePaperUpload.vue')
    },
    {
      path: '/Payment',
      name: 'payment',
      component: () => import('./views/Payment2.vue')
    },
    {
      path: '/EventsReport',
      name: 'eventsReport',
      component: () => import('./views/EventsReport.vue')
    },
    {
      path: '/InvitationEventDetail/:id',
      name: 'invitationEventDetail',
      component: () => import('./views/InvitationEventDetail.vue')
    },
    {
      path: '/SendInvitation/',
      name: 'sendInvitation',
      component: () => import('./views/SendInvitation.vue')
    }
    //,
    //{ path: '*', redirect: '/' }
  ]
});

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  var p = to.path.split('/');
  var path = '';
  if(p.length>1){
    path='/'+p[1];
  }else{
    path=to.path;
  }
  const publicPages = ['/', '/Login', '/Register','/TimeConfig','/InvitationEventDetail', '/EventsReport'];
  const authRequired = ! publicPages.includes(path);
  const loggedIn = localStorage.getItem('user');
  if (authRequired && !loggedIn) {
    return next('/');
  }
  next();
})

export default router;