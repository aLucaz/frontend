/*
Entorno de desarrollo (no para produccion)

Now we want to configure our development server to proxy API request to the backend domain
The proxy property is a string of the backend host
 
*/
module.exports = {
    devServer: {
        proxy: 'http://127.0.0.1:5000/'
    }
}